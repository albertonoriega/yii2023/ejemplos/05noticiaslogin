<?php

namespace app\controllers;

use app\models\Autor;
use app\models\LoginForm;
use app\models\Noticia;
use app\models\Seccion;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Mostrar las noticias de portada
     *
     * @return string
     */
    public function actionIndex()
    {
        // Necesito recuperar todas las noticias de la BBDD
        $consulta = Noticia::find();

        //$datos = $consulta->all(); // Las noticias estan en un array de modelos
        //$datos1 = $consulta->asArray()->all(); // Las noticias están en un array de arrays

        $datos = $consulta
            ->where(['portada' => 1]) // seleccionar
            ->all(); // ejecutar
        return $this->render('index', [
            'datos' => $datos,
            'titulo' => 'Noticias de portada'
        ]);
    }

    public function actionVernoticia($idNoticia)
    {
        $consulta = Noticia::find()->where(['idNoticia' => $idNoticia]);

        $dato = $consulta->one();

        return $this->render('verNoticia', [
            'dato' => $dato,
        ]);
    }

    public function actionSecciones()
    {
        $secciones = Seccion::find()->all();

        return $this->render('secciones', [
            'secciones' => $secciones
        ]);

        // return $this->render('secciones', compact('secciones));
    }

    public function actionSeccion($id)
    {
        $datos = Noticia::find()->where(['seccion' => $id])->all();

        // Hacemos la consulta para obtener el nombre de la seccion seleccionada
        $seccion = Seccion::find()->where(['id' => $id])->one();

        //$seccion = Seccion::findOne($id); // Esto hace lo mismo pero solo vale cuando filtras por el primary key

        return $this->render("index", [
            "datos" => $datos,
            "titulo" => "Noticias de " . $seccion->nombre,
        ]);
    }

    public function actionAutores()
    {
        $autores = Autor::find()->all();

        return $this->render('autores', [
            'autores' => $autores
        ]);
        // return $this->render('autores', compact('autores));
    }

    public function actionAutor($id)
    {
        $datos = Noticia::find()->where(['autor' => $id])->all();

        // Hacemos la consulta para obtener el nombre de la seccion seleccionada
        $autor = Autor::find()->where(['id' => $id])->one();


        return $this->render("index", [
            "datos" => $datos,
            "titulo" => "Noticias de " . $autor->nombre
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
