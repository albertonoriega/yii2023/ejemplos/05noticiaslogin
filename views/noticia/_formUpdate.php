<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Noticia $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="noticia-form">

    <?php $form = ActiveForm::begin(); ?>
    <!-- Hacemos que el id al actualizar no se pueda modificar y para eso usamos readonly -->
    <?= $form->field($model, 'idNoticia')->input('number', ['readonly' => 'readonly']) ?>

    <?= $form->field($model, 'titular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'textoCorto')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'textoLargo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'portada')->checkbox() ?>

    <?= $form->field($model, 'seccion')->dropDownList($model->secciones, ['prompt' => 'Selecciona sección']) ?>

    <?= $form->field($model, 'fecha')->input('datetime-local') ?>

    <?= $form->field($model, 'fichero')->fileInput() ?>

    <?= Html::img('@web/imgs/noticias/' . $model->foto, ['class' => 'col-lg-2']) ?>

    <?= $form->field($model, 'autor')->listBox($model->autores) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>