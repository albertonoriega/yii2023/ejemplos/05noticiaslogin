<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Noticia $model */

$this->title = $model->titular;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="noticia-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idNoticia' => $model->idNoticia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idNoticia' => $model->idNoticia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idNoticia',
            'titular',
            'textoCorto',
            'textoLargo:ntext',
            //'portada:boolean',
            [
                'attribute' => 'portada',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->portada == 1 ? '<i class="fas fa-check-circle" style="font-size:2em;color:green"></i>' : '<i class="fas fa-times-circle" style="font-size:2em;color:red"></i>';
                }
            ],
            //'seccion',
            [
                'attribute' => 'Sección',
                'value' => function ($model) {
                    if (isset($model->seccion0->nombre)) {
                        return $model->seccion0->nombre;
                    }
                }
            ],
            //'fecha',
            [
                'attribute' => 'fecha',
                'value' => function ($model) {
                    return $model->fechaEspana();
                }
            ],
            //'foto',
            [
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if ($dato->foto != null) {
                        return Html::img("@web/imgs/noticia/{$dato->foto}", ["width" => 300, "height" => 300]);
                    } else {
                        return Html::img("@web/imgs/secciones/notfound.png", ["width" => 300, "height" => 300]);
                    }
                }
            ],
            //'autor',
            [
                'attribute' => 'Autor',
                'value' => function ($model) {
                    if (isset($model->autor0->nombre)) {
                        return $model->autor0->nombre;
                    }
                }
            ],
        ],
    ]) ?>

</div>