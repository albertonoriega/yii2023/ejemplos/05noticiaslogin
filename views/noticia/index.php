<?php

use app\models\Noticia;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="far fa-plus-square"></i> <i class="fas fa-newspaper"></i> Nueva noticia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idNoticia',
            'titular',
            'textoCorto',
            //'textoLargo:ntext',
            //'portada:boolean',
            //'seccion',
            //'fecha',
            //'foto',
            //'autor',
            [
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if ($dato->foto != null) {
                        return Html::img("@web/imgs/noticias/{$dato->foto}", ["width" => 200, "height" => 200]);
                    } else {
                        return Html::img("@web/imgs/secciones/notfound.png", ["width" => 200, "height" => 200]);
                    }
                }
            ],
            [
                'attribute' => 'portada',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->portada == 1 ? '<i class="fas fa-check-circle" style="font-size:3em;color:green"></i>' : '<i class="fas fa-times-circle" style="font-size:3em;color:red"></i>';
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Noticia $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idNoticia' => $model->idNoticia]);
                }
            ],
        ],
    ]); ?>


</div>