<?php

use yii\helpers\Html;

?>

<div class="jumbotron text-center bg-transparent mt-5 mb-5 tituloPortada">
    <?= Html::img('@web/imgs/NoticiasPortada.jpg', ['class' => 'img-thumbnail', 'width' => '225px']);   ?>
    <h1 class="display-4">Secciones</h1>
    <?= Html::img('@web/imgs/NoticiasPortada.jpg', ['class' => 'img-thumbnail', 'width' => '225px']);   ?>
</div>

<div class="row">
    <?php
    foreach ($secciones as $seccion) {
    ?>
        <div class="col-4 mb-3">
            <div class="card bg-primary text-white cartas">
                <div class="imagenCard">
                    <?php
                    if (isset($seccion->foto)) {
                        echo Html::img("@web/imgs/secciones/{$seccion->foto}", ["width" => 100, "height" => 100, 'style' => 'border-radius:50%;margin:5px']);
                    } else {
                        echo Html::img("@web/imgs/secciones/notfound.png", ["width" => 100, "height" => 100, 'style' => 'border-radius:50%;margin:5px']);
                    }
                    ?>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?= $seccion->nombre ?></h5>
                    <?= Html::a('Ver noticias', ['site/seccion', 'id' => $seccion->id], ['class' => 'btn btn-light']) ?>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>