<?php

use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => '', 'url' => ['/site/index'], 'linkOptions' => ['class' => "fas fa-home", 'style' => 'font-size:1.5em']],
        ['label' => 'Secciones', 'url' => ['/site/secciones']],
        ['label' => 'Autores', 'url' => ['/site/autores']],
        ['label' => 'Administración', 'items' => [
            ['label' => 'Noticias', 'url' => ['/noticia/index']],
            ['label' => 'Seciones', 'url' => ['/seccion/index']],
            ['label' => 'Autores', 'url' => ['/autor/index']],
        ]],
        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Cerrar sesión (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
