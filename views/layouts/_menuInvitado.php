<?php

use yii\bootstrap5\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => '', 'url' => ['/site/index'], 'linkOptions' => ['class' => "fas fa-home", 'style' => 'font-size:1.5em']],
        ['label' => 'Secciones', 'url' => ['/site/secciones']],
        ['label' => 'Autores', 'url' => ['/site/autores']],
        ['label' => ' Login ', 'url' => ['/site/login'], 'linkOptions' => ['class' => "fas fa-user", 'style' => 'position:relative;top:5px']],
    ]
]);
