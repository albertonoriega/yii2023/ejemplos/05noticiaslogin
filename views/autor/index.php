<?php

use app\models\Autor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="far fa-plus-square"></i> Nuevo autor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            //'foto',
            [
                'label' => 'Foto',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if ($dato->foto != null) {
                        return Html::img("@web/imgs/autores/{$dato->foto}", ["width" => 200, "height" => 200]);
                    } else {
                        return Html::img("@web/imgs/autores/anonimo.png", ["width" => 200, "height" => 200]);
                    }
                }
            ],
            //'fechaNacimiento',
            [
                'attribute' => 'fechaNacimiento',
                'value' => function ($model) {
                    return $model->fechaEspana();
                }
            ],
            'correo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Autor $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>